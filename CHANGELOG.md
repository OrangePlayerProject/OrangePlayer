- 0.1-Beta

  - Se construyen clases
  - Se prueba conexion con servidor

- 0.2-Beta

  - Se corrige error de mensajes en actividad principal - Se corrige error con icono play/pause al pulsarlo (iconos cambiados) - Se define imagen por defecto para canciones sin cover

- 0.3-Beta

  - Arquitectura modificada y hecha de cero, ahora se conecta a un web service en spring boot.

- 0.4-Beta

  - Se configuran mas componentes pero aun falta mejorar la conexion con el servicio web
  - Arquitectura migrada a androidx

- 1.0-Beta

  - Reconstruccion completa en Flutter, se agregan algunas funcionalidades pero falta mas

- 2.0-Beta

  - Se agrega difuminado de fondos
  - Se modifican algunos nombre de clases y estructura de carpetas
  - Se agregan componentes personalizados para ciertas tareas.
  - Se crea una interfaz flexible, que permite una mejor adaptacion a distintas pantallas, fala mejorar

- 2.1-Beta

  - Se agrega splashscreen
  - Agregada funcion de apagado de maquina remota
  - Se mejora usabilidad de sync widget
  - Opcion para salir sin apagar maquina en menu
  - Cambio de iconos en menu

- 2.2-Beta

  - Reordenamiento de paquetes de codigo
  - Cambio de diseño
  - Mejorando interfaz de usuario

- 2.3-Beta
  - Se mejora interfaz
  - Se implementa modo fullscreen en algunas pantallas
  - Reordenamiento de algunas partes del codigo
  - Se corrige error con boton play
  - Falta corregir error de cover

- 2.4-Beta
  - Correccion de pagina player
  - Algunas clases son reordenadas en paquetes y nomenclatura

- 2.5-Beta
  - Carga de cancion actual optimizada
