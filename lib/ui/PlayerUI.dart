import 'package:flutter/material.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/pages/components/blur_image.dart';
import 'package:orange_player/pages/player/track_cover.dart';
import 'package:orange_player/pages/player/track_progress_bar.dart';

class PlayerUI {
  BlurImage background;
  TrackCover cover;
  Text lblTitle;
  Text lblArtist;
  Text lblProgress;
  Text lblDuration;
  TrackProgressBar progressBar;

  MusicConsumer musicConsumer;

  PlayerUI(
      {BlurImage background,
      TrackCover cover,
      Text lblTitle,
      Text lblArtist,
      Text lblProgress,
      Text lblDuration,
      TrackProgressBar progressBar}) {
    this.background = background;
    this.cover = cover;
    this.lblTitle = lblTitle;
    this.lblArtist = lblArtist;
    this.lblProgress = lblProgress;
    this.lblDuration = lblDuration;
    this.progressBar = progressBar;

    musicConsumer = MusicConsumer();
  }
}
