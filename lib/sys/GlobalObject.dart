import 'dart:typed_data';

import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/net/beans/Song.dart';
import 'package:orange_player/sys/SysInfo.dart';

class GlobalObject {
  static Future<Song> currentSong;
  static Future<Uint8List> currentCover;

  static void setGlobalObjectsDefault() {
    setGlobalObjects(
        MusicConsumer(host: SysInfo.SERVER_HOST, port: SysInfo.SERVER_PORT));
  }

  static void setGlobalObjects(MusicConsumer consumer) {
    GlobalObject.currentSong = consumer.getCurrentNoCover();
    GlobalObject.currentCover = consumer.getCurrentCover();
  }

  static void setGlobalObjectsDefaultAsync() {
    Future.delayed(Duration(milliseconds: 100), () {
      setGlobalObjectsDefault();
    });
  }

  static void setGlobalObjectsAsync(MusicConsumer consumer) {
    Future.delayed(Duration(milliseconds: 100), () {
      setGlobalObjects(consumer);
    });
  }
}
