import 'dart:ui';

class SysInfo {
  static const Color PRIMARY_COLOR = Color(0xffFF3D00);
  static const Color ACCENT_COLOR = Color(0xffC2C2C2);
  static const String APP_NAME = "Orange Player";

  static String SERVER_HOST = "192.168.56.1" // geny nat
      //"192.168.0.24" // wifi home
      //"192.168.1.9" // wifi movistar
      //"192.168.0.41" // raspi home
      //"192.168.42.150" // wifi share mate 20 y ethernet remix
      //"192.168.43.10" // wifi share remix
      //"10.42.0.1" // wifi note
      //"localhost" // for testings
      ;
  static int SERVER_PORT = 40000;
}
