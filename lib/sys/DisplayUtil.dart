import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// 80 -> bar height
class DisplayUtil {
  static Size _getDisplaySize(BuildContext context, {bool withBar = true}) {
    var size = MediaQuery.of(context).size;
    return Size(size.width, withBar ? size.height - 80 : size.height);
  }

  static double getWidth(BuildContext context, {double perc = 100}) {
    double width = _getDisplaySize(context).width;
    return width * (perc / 100);
  }

  static double getWidthFromParent(double parentWidth, {double perc = 100}) {
    return parentWidth * (perc / 100);
  }

  static double getHeight(BuildContext context,
      {double perc = 100, bool withBar = true}) {
    double height = _getDisplaySize(context, withBar: withBar).height;
    return height * (perc / 100);
  }

  static double getHeightFromParent(double parentHeight, {double perc = 100}) {
    return parentHeight * (perc / 100);
  }

  static void enterFullscreen() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  static void hideStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  }

  static void hideBottomBar() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
  }

  static void showAllBars() {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top, SystemUiOverlay.bottom]);
  }
}
