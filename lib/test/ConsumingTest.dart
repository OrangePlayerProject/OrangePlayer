import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/net/beans/Song.dart';

void main(List<String> args) {
  testMusic();
}

Future testMusic() async {
  MusicConsumer consumer = new MusicConsumer(
    host: "localhost");
  Future<List<Song>> future = consumer.getSongs();
  print((await future).toString());
}
