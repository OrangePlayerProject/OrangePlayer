import 'dart:typed_data';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class URLConnector {
  String host;
  int port;

  URLConnector({String host, int port}) {
    this.host = host;
    this.port = port;
  }

  String openUrl(String path, List<String> parameters) {
    StringBuffer sbUrl = StringBuffer();
    sbUrl.write("http://");
    sbUrl.write(host);
    sbUrl.write(':');
    sbUrl.write(port);
    sbUrl.write(path);
    sbUrl.write('/');

    String encoded;
    parameters.forEach((param) {
      encoded = Uri.encodeFull(param);
      encoded = encoded.replaceAll("/", "%2F");
      sbUrl.write(encoded);
      sbUrl.write('/');
    });
    String url = sbUrl.toString();
    print("URL: $url");
    return url;
  }

  Future<Uint8List> getUrlData(String path, List<String> parameters) async {
    Future<http.Response> future = http.get(openUrl(path, parameters));
    return (await future).bodyBytes;
  }

  // esta la dejare asincrona para el posible error de threadonmainexception
  // de android
  Future<String> getUrlContent(String path, List<String> parameters) async {
    http.Response response = await http.get(openUrl(path, parameters));
    String content = response.body;
    print("URLContent: $content");
    return content;
  }

  Future sendRequest(String path, List<String> parameters) async {
    http.Response response = await http.get(openUrl(path, parameters));
  }
}
