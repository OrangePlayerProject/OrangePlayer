class PlayerResponse {
  int code;
  var response;

  factory PlayerResponse.fromJson(Map<String, dynamic> json) {
    return PlayerResponse(code: json["code"], response: json["response"]);
  }

  PlayerResponse({int code, Map<String, dynamic> response}) {
    this.code = code;
    this.response = response;
  }

  @override
  String toString() {
    StringBuffer buffer = StringBuffer();
    buffer.writeln('{');
    buffer.write("\tcode: ");
    buffer.write(code);
    buffer.writeln(',');
    buffer.write("\tresponse: ");
    buffer.write(response.toString());
    buffer.writeln(',');
    buffer.write('}');
    return buffer.toString();
  }
}
