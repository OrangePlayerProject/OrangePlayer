class ResponseCode {
  static final int PLAYER_SHUTDOWN = 100;
  static final int OK_RESPONSE = 101;
  static final int ERROR_RESPONSE = 102;
}