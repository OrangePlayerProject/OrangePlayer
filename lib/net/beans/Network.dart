import 'dart:core';

class Network {
    String name;

    static Network getNetwork(String cmdLine) {
        List<String> lineSplit = cmdLine.split(":");
        final String wifiName = lineSplit[1];
        return new Network(
            name: lineSplit[1].substring(1, wifiName.length-1)
        );
    }

    factory Network.fromJson(Map<String, dynamic> json) {
        return Network(name: json["name"]);
    }

    Network({String name}) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    @override String toString() {
        return name;
    }
}