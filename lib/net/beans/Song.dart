import 'dart:io';
import 'dart:typed_data';

class Song {
  String title;
  String album;
  String artist;
  String date;
  String progress;
  String duration;
  Uint8List coverData;
  //int volume;

  int trackIndex;

  factory Song.fromJson(Map<String, dynamic> json) {
    var cover = json["coverData"];
    return Song(
        album: json["album"],
        artist: json["artist"],
        coverData: decodeCoverData(cover),
        date: json["date"],
        duration: json["duration"],
        title: json["title"],
        trackIndex: json["trackIndex"],
        progress: json["progress"]);
  }

  Song(
      {String title,
      String album,
      String artist,
      String date,
      String duration,
      String progress,
      Uint8List coverData,
      int trackIndex}) {
    this.title = title;
    this.album = album;
    this.artist = artist;
    this.date = date;
    this.duration = duration;
    this.progress = progress;
    this.coverData = coverData;
    this.trackIndex = trackIndex;
  }

  static Uint8List decodeCoverData(List<dynamic> coverBytes) {
    Uint8List toReturn;
    if (coverBytes == null) {
      toReturn = Uint8List(0);
    } else {
      List<int> listInts = List.from(coverBytes);
      Uint8List listBytes = Uint8List.fromList(listInts);
      toReturn = listBytes;
    }
    return toReturn;
  }

  bool hasCover() {
    return coverData != null && coverData.length > 0;
  }

  @override
  String toString() {
    StringBuffer buffer = new StringBuffer();
    buffer.write("Song{\n");
    buffer.write("title='${title == null ? "Titulo Desconocido" : title}'\n");
    buffer.write(", album='${album == null ? "Album Desconocido" : album}'\n");
    buffer.write(
        ", artist='${album == null ? "Artista Desconocido" : artist}'\n");
    buffer.write(", date='${album == null ? "Date Desconocido" : date}'\n");
    buffer.write(", progress='$progress'\n");
    buffer.write(", duration='$duration'\n");
    buffer.write(
        ", coverData='${coverData == null ? "No Cover" : "Has Cover"}'\n}");
    return buffer.toString();
  }
}
