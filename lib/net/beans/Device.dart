import 'dart:io';

class Device {
    String name;
    String label;
    String mountPoint;
    File file;

    factory Device.fromJson(Map<String, dynamic> json) {
        var jsonFile = json["file"];
        return Device(
          name: json["name"],
          label: json["label"],
          mountPoint: json["mountPoint"],
          file: jsonFile == null ? null : File(jsonFile)
      );
    }

    Device({String name, String label, String mountPoint, File file}) {
        this.name = name;
        this.label = label;
        this.mountPoint = mountPoint;
        this.file = file;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getLabel() {
        return label;
    }

    void setLabel(String label) {
        this.label = label;
    }

    String getMountPoint() {
        return mountPoint;
    }

    void setMountPoint(String mountPoint) {
        this.mountPoint = mountPoint;
    }

    File getFile() {
        return file;
    }

    void setFile(File file) {
        this.file = file;
    }
    
    @override String toString() {
        return "Device: "+getFile().path;
    }
}
