import 'dart:io';

import 'package:orange_player/sys/SysInfo.dart';

import 'Consumer.dart';
import 'beans/Device.dart';

class SystemConsumer extends Consumer {
  SystemConsumer({String host, int port})
      : super(
            host: host == null ? SysInfo.SERVER_HOST : host,
            port: port == null ? SysInfo.SERVER_PORT : port,
            servicePath: "/sys");

  Future<bool> mount(String devFileName) {
    return consume("/mount", [devFileName]).then((value) => value == 'true');
  }

  Future<bool> umount(String devFileName) {
    return consume("/umount", [devFileName]).then((value) => value == 'true');
  }

  Future<List<Device>> getDevices() async {
    List<dynamic> list = await consumeList("/getdevs", []);
    return list.map((dev) => Device.fromJson(dev)).toList();
  }

  Future<List<Device>> getMounted() async {
    List<dynamic> list = await consumeList("/getmounted", []);
    return list.map((dev) => Device.fromJson(dev)).toList();
  }

  Future<List<String>> getLabels() async {
    List<dynamic> list = await consumeList("/getlabels", []);
    return list.map((label) => label).toList();
  }

  Future<List<File>> listFiles(String folderPath) async {
    List<dynamic> list = await consumeList("/listfiles", []);
    return list.map((file) => File(file)).toList();
  }

  Future shutdownSystem() {
    consumeNothing("/shutdown", []);
  }
}
