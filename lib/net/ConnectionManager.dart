import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:orange_player/net/URLConnector.dart';
import 'package:orange_player/sys/SysInfo.dart';

class ConnectionManager {
  static Future<bool> isConnected(String host) async {
    Connectivity connectivity = Connectivity();
    //ConnectivityResult conResult = await Connectivity().checkConnectivity();
    //print("ConResult: ${conResult.toString()}");
    if (!await isWifiOn(connectivity)) {
      return false;
    }
    URLConnector connector = URLConnector(
      host: host,
      port: SysInfo.SERVER_PORT,
    );
    try {
      await connector.getUrlContent("", []);
      return true;
    } catch (e) {
      print("NetworkError: ${e.toString()}");
      return false;
    }
  }

  static Future<bool> isWifiOn(Connectivity con) async {
    return await con.getWifiIP() != null;
  }

  static Future<bool> isConnectedDefault() async {
    return isConnected(SysInfo.SERVER_HOST);
  }

  static String getNetworkAddress() {
    return null;
  }
}
