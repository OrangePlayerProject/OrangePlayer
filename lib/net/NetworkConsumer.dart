import 'dart:typed_data';

import 'package:orange_player/net/beans/Network.dart';
import 'package:orange_player/sys/SysInfo.dart';

import 'Consumer.dart';

class NetworkConsumer extends Consumer {
  NetworkConsumer({Future<String> host, int port})
      : super(
            host: host == null ? SysInfo.SERVER_HOST : host,
            port: port == null ? SysInfo.SERVER_PORT : port,
            servicePath: "/net");

  Future<bool> connect() async {
    return (await consumeString("/link", [])).trim().toLowerCase() == 'true';
  }

  Future<bool> disconnect() async {
    return (await consumeString("/unlink", [])).trim().toLowerCase() == 'true';
  }

  Future<bool> isConnected() async {
    return (await consumeString("/islinked", [])).trim().toLowerCase() ==
        'true';
  }

  Future<bool> connectWifi(String ssid, String passw) async {
    return (await consumeString("/connect", [ssid, passw]))
            .trim()
            .toLowerCase() ==
        'true';
  }

  Future<String> getEthernetIp() async {
    return (await consumeString("/ethip", []));
  }

  Future<String> getWlanIp() async {
    return (await consumeString("/wlanip", []));
  }

  Future<List<Network>> getNetworks() async {
    List<dynamic> list = (await consumeList("/getnetworks", []));
    return list.map((net) => Network.fromJson(net)).toList();
  }
}
