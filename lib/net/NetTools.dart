//import 'package:orange_player/sys/SysInfo.dart';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:executor/executor.dart';
//import 'package:connectivity/connectivity.dart';

class NetTools {
  static Future<bool> _isServer(String ip) async {
    try {
      await http.get("http://$ip:40000");
      return true;
    } catch (e) {
      return false;
    }
  }

  static Future<String> findServerIp() async {
    Connectivity con = Connectivity();
    String ip = await con.getWifiIP();
    String networkAddr = ip.substring(0, ip.lastIndexOf("."));

    StringBuffer sbIp = new StringBuffer();
    Executor executor = Executor(concurrency: 253);

    String server = null;
    bool isServer = false;
    String currentIp;

    for (var i = 2; i < 255; i++) {
      print("FOR: $i");
      sbIp.write(networkAddr);
      sbIp.write(".");
      sbIp.write(i.toString());
      executor.scheduleTask((() async {
        if (!isServer) {
          server = sbIp.toString();
        }
        isServer = await _isServer(sbIp.toString());
      }));
      if (isServer) {
        break;
      }
      sbIp.clear();
    }
    while (executor.waitingCount > 0) {
      print("Waiting to ${executor.waitingCount} threads");
    }
    return server;
  }
}

main(List<String> args) async {
  print(await NetTools.findServerIp());
}
