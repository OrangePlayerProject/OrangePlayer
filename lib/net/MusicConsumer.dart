import 'dart:typed_data';

import 'package:orange_player/net/Consumer.dart';
import 'package:orange_player/net/beans/Song.dart';
import 'dart:async';

import 'package:orange_player/sys/SysInfo.dart';
import 'package:path/path.dart';

class MusicConsumer extends Consumer {
  MusicConsumer({String host, int port})
      : super(
            host: host == null ? SysInfo.SERVER_HOST : host,
            port: port == null ? SysInfo.SERVER_PORT : port,
            servicePath: "/music");

  Future<bool> isPlayerStarted() {
    Future<String> future =
        connector.getUrlContent(formatPath("/isstarted"), []);
    return future.then((content) => content.toLowerCase() == 'true');
  }

  Future<bool> isPlayerPaused() {
    Future<String> future =
        connector.getUrlContent(formatPath("/ispaused"), []);
    return future.then((content) => content.toLowerCase() == 'true');
  }

  Future<bool> isPlayerStopped() {
    Future<String> future =
        connector.getUrlContent(formatPath("/isstopped"), []);
    return future.then((content) => content.toLowerCase() == 'true');
  }

  Future<bool> isPlayerPlaying() {
    Future<String> future =
        connector.getUrlContent(formatPath("/isplaying"), []);
    return future.then((content) => content.toLowerCase() == 'true');
  }

  Future<bool> startPlayer() {
    Future<String> future = connector.getUrlContent(formatPath("/start"), []);
    return future.then((content) => content.toLowerCase() == 'true');
  }

  void play(List<String> path) {
    consumeNothing("/play", path);
  }

  void playFolder(String fldPath, bool nameOnly) {
    if (nameOnly) fldPath = basename(fldPath);
    consumeNothing("/playfld", [fldPath]);
  }

  void stop() {
    consumeNothing("/stop", []);
  }

  void resume() {
    consumeNothing("/resume", []);
  }

  void pause() {
    consumeNothing("/pause", []);
  }

  void next() {
    consumeNothing("/next", []);
  }

  void prev() {
    consumeNothing("/prev", []);
  }

  void mute() {
    consumeNothing("/mute", []);
  }

  void unmute() {
    consumeNothing("/unmute", []);
  }

  Future<double> getGain() {
    Future<String> future = connector.getUrlContent(formatPath("/getgain"), []);
    return future.then((content) => double.parse(content));
  }

  void setGain(double volume) {
    consumeNothing("/setgain", [volume.round().toString()]);
  }

  void shutdown() {
    consumeNothing("/shutdown", []);
  }

  Future<Song> getCurrentInfo() {
    return consume("/current", []).then((current) => Song.fromJson(current));
  }

  Future<Song> getCurrentNoCover() {
    return consume("/current/nocover", [])
        .then((current) => Song.fromJson(current));
  }

  Future<Uint8List> getCurrentCover() async {
    return await consumeBytes("/currentcover", []);
  }

  Future<List<String>> listFolders() async {
    List<dynamic> list = (await consumeList("/folders", []));
    return list.map((fld) => fld.toString()).toList();
  }

  Future<List<Song>> getSongs() async {
    List<dynamic> list = (await consumeList("/list", []));
    return list.map((song) => Song.fromJson(song)).toList();
  }

  Future<List<Song>> getFullSongs() async {
    List<dynamic> list = (await consumeList("/listfull", []));
    var list2 = list.map((song) => Song.fromJson(song)).toList();

    return list2;
  }
}
