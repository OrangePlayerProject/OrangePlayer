import 'dart:convert';
import 'dart:typed_data';

import 'package:orange_player/net/URLConnector.dart';
import 'package:orange_player/net/beans/PlayerResponse.dart';
import 'package:orange_player/sys/SysInfo.dart';

abstract class Consumer {
  URLConnector connector;

  String _servicePath;

  Consumer({String host, int port, String servicePath}) {
    connector = URLConnector(
        host: host, port: port == null ? SysInfo.SERVER_PORT : port);
    _servicePath = servicePath;
  }

  String formatPath(String urlPath) {
    return _servicePath + urlPath;
  }

  Future<Map<String, dynamic>> consume(
      String urlPath, List<String> parameters) async {
    // return then es lo mismo que return con await solo que en este ultimo
    // se convierte la funcion en asincrona
    String urlContent =
        await connector.getUrlContent(formatPath(urlPath), parameters);

    return jsonDecode(urlContent);
  }

  Future<Uint8List> consumeBytes(
      String urlPath, List<String> parameters) async {
    var data = await connector.getUrlData(formatPath(urlPath), parameters);
    if (data.length == 0) {
      data = null;
    }
    return data;
  }

  Future<List<dynamic>> consumeList(
      String urlPath, List<String> parameters) async {
    String urlContent =
        await connector.getUrlContent(formatPath(urlPath), parameters);
    return jsonDecode(urlContent);
  }

  Future<String> consumeString(String urlPath, List<String> parameters) async {
    return await connector.getUrlContent(formatPath(urlPath), parameters);
  }

  void consumeNothing(String urlPath, List<String> parameters) async {
    await connector.sendRequest(formatPath(urlPath), parameters);
  }

  // con await se juntan los bloques de futures en uno solo
  /*Future<PlayerResponse> consumeResponse(String urlPath, List<String> parameters) async {
    return PlayerResponse.fromJson(await consume(urlPath, parameters));
  }*/

}
