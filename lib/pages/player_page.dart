import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/net/beans/Song.dart';
import 'package:orange_player/sys/DisplayUtil.dart';
import 'package:orange_player/pages/player/panel_buttons_page.dart';
import 'package:orange_player/pages/player/track_cover.dart';
import 'package:orange_player/pages/player/track_progress_bar.dart';
import 'package:orange_player/sys/GlobalObject.dart';

import 'components/blur_image.dart';

class PlayerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PlayerPageState();
  }
}

// cualquier hijo de materialApp puede acceder a instancias de navigator

class PlayerPageState extends State<PlayerPage> {
  // el scaffold es un widget que usa material design
  double _sliderVal = 40;
  String _sliderLabel = "40";

  MusicConsumer consumer;
  Future<Song> currentSong;
  Future<Uint8List> currentCover;

  void loadSong() async {
    setState(() {
      currentSong = GlobalObject.currentSong;
      currentCover = GlobalObject.currentCover;
    });
  }

  @override
  void initState() {
    super.initState();
    DisplayUtil.showAllBars();
    consumer = MusicConsumer();
    loadSong();
  }

  @override
  Widget build(BuildContext context) {
    /*double width = DisplayUtil.getWidth(context);
    double heigth = DisplayUtil.getHeight(context);
    double imgHeight = 40;
    double _thumbPercent = 0.4;*/

    // TODO: implement build

    return Scaffold(
      primary: true,
      body: FutureBuilder(
        future: currentSong,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            return getElements(
              current: snapshot.data,
            );
          } else {
            return getElements();
          }
        },
      ),
    );
  }

  Widget getElements({Song current}) {
    final String defaultCoverBg = "assets/img/cover.png";
    final String defaultCoverPath = "cd.png";

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        FutureBuilder(
          future: currentCover,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              return BlurImage(
                imgSrc: snapshot.data == null ? defaultCoverBg : snapshot.data,
              );
            } else {
              return BlurImage(
                imgSrc: defaultCoverBg,
              );
            }
          },
        ),
        Positioned(
          child: Align(
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).size.height / 8,
                    ),
                  ),
                  Container(
                    height: 200,
                    width: 200,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        FutureBuilder(
                          future: currentCover,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                    ConnectionState.done &&
                                snapshot.hasData) {
                              return TrackCover(
                                imgSource: snapshot.data == null
                                    ? defaultCoverPath
                                    : snapshot.data,
                              );
                            } else {
                              return TrackCover(
                                imgSource: defaultCoverPath,
                              );
                            }
                          },
                        ),
                        TrackProgressBar(this),
                      ],
                    ),
                  ),
                  Container(
                    margin:
                        EdgeInsets.only(bottom: 0, left: 0, right: 0, top: 40),
                    width: DisplayUtil.getWidth(context, perc: 95),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          current == null
                              ? "Título Desconocido"
                              : current.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            shadows: _getTextShadows(),
                          ),
                        ),
                        Container(
                          height: 20,
                        ),
                        Text(
                          current != null && current.artist != null
                              ? current.artist
                              : "Artista Desconocido",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            shadows: _getTextShadows(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height:
                        DisplayUtil.getHeight(context, perc: 6, withBar: false),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        width: DisplayUtil.getWidth(context, perc: 15),
                        margin: EdgeInsets.only(
                            left: DisplayUtil.getWidth(context, perc: 3)),
                        child: Text(
                          "00:00",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            shadows: _getTextShadows(),
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        width: DisplayUtil.getWidth(context, perc: 64),
                        child: Slider(
                          value: _sliderVal,
                          max: 100,
                          divisions: 100,
                          inactiveColor: Colors.black,
                          activeColor: Colors.white,
                          label: _sliderLabel,
                          onChanged: (double e) => changed(e),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        width: DisplayUtil.getWidth(context, perc: 15),
                        margin: EdgeInsets.only(
                            left: DisplayUtil.getWidth(context, perc: 3)),
                        child: Text(
                          current == null ? "00:00" : current.duration,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            shadows: _getTextShadows(),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: DisplayUtil.getHeight(
                        context,
                        perc: 2,
                        withBar: false,
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    child: PanelButtonsWidget(this),
                  ),
                ],
              )),
        )
      ],
    );
  }

  void changed(e) {
    consumer.setGain(e);
    setState(() {
      _sliderVal = e;
      _sliderLabel = (e as double).round().toString();
    });
  }

  List<Shadow> _getTextShadows() {
    return <Shadow>[
      Shadow(
        offset: Offset(1, 1),
        blurRadius: 3.0,
        color: Color.fromARGB(255, 0, 0, 0),
      ),
      Shadow(
        offset: Offset(1, 1),
        blurRadius: 3.0,
        color: Color.fromARGB(255, 0, 0, 0),
      ),
    ];
  }
}
