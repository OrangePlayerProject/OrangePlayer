import 'package:flutter/material.dart';

class Separator extends StatelessWidget{
  
  double _width;
  double _height;

  Separator({double width, double height}) {
      this._width = width;
      this._height = height;
  } 
    
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
      return SizedBox(
        height: _height,
        width: _width,
      );
  }

}