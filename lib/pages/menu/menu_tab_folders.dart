import 'package:flutter/material.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:path/path.dart';

class MenuTab2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MenuTab2State();
  }
}

// -------------- STATE -----------------

class MenuTab2State extends State<MenuTab2>
    with AutomaticKeepAliveClientMixin<MenuTab2> {
  MusicConsumer consumer;
  Future<List<String>> listFolders;

  MenuTab2State() {
    consumer = MusicConsumer();
    listFolders = consumer.listFolders();
  }

  Widget getListElement(String fldPath, {BuildContext context}) {
    final int titleLimit = 40;
    String fldName = basename(fldPath);
    if (fldName.length > titleLimit) {
      fldName = fldName.substring(0, titleLimit);
    }

    return Card(
      borderOnForeground: true,
      elevation: 3,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 5, top: 5),
      child: InkWell(
        onTap: () async {
          await consumer.playFolder(fldPath, true);
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 10, right: 20),
                width: 40,
                height: 40,
                child: Image.asset("assets/img/folder.png")),
            Text(
              fldName,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> getListWidgets(
      {List<String> listFolders, BuildContext context}) {
    if (listFolders == null)
      return List();
    else {
      return listFolders.map((folder) {
        return getListElement(folder, context: context);
      }).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Align(
      alignment: Alignment.center,
      child: FutureBuilder(
        future: listFolders,
        builder: (context, response) {
          ConnectionState state = response.connectionState;
          if (state == ConnectionState.done && response.hasData) {
            List<String> listFld = response.data;
            return ListView.builder(
              itemBuilder: (ctx, pos) {
                return getListElement(listFld[pos], context: ctx);
              },
              itemCount: listFld.length,
            );
          } else {
            if (response.hasError) {
              print('ResponseError: ${response.error}');
              return ListView(
                children: getListWidgets(),
              );
            } else {
              return CircularProgressIndicator();
            }
          }
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
