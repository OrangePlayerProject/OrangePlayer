import 'package:flutter/material.dart';
import 'package:orange_player/net/SystemConsumer.dart';
import 'package:orange_player/net/beans/Device.dart';
import 'package:orange_player/pages/components/waiting_bar.dart';

class TabDrives extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TabDrivesState();
  }
}

// -------------- STATE -----------------

class _TabDrivesState extends State<TabDrives>
    with AutomaticKeepAliveClientMixin<TabDrives> {
  SystemConsumer consumer;
  Future<List<Device>> listDevs;

  _TabDrivesState() {
    consumer = SystemConsumer();
    listDevs = consumer.getDevices();
  }

  Widget getListElement(Device dev, {BuildContext context}) {
    final int titleLimit = 40;
    String devName = dev.label;
    if (devName.length > titleLimit) {
      devName = devName.substring(0, titleLimit);
    }

    return Card(
      borderOnForeground: true,
      elevation: 3,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 5, top: 5),
      child: InkWell(
        onTap: () async {
          consumer.mount(dev.name).then((mounted) {});
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 10, right: 20),
                width: 40,
                height: 40,
                child: Image.asset("assets/img/pendrive1.png")),
            Text(
              devName,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> getListWidgets({List<Device> listDevs, BuildContext context}) {
    if (listDevs == null)
      return List();
    else {
      return listDevs.map((dev) {
        return getListElement(dev, context: context);
      }).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Align(
      alignment: Alignment.center,
      child: FutureBuilder(
        future: listDevs,
        builder: (context, response) {
          ConnectionState state = response.connectionState;
          if (state == ConnectionState.done && response.hasData) {
            List<Device> listDevs = response.data;
            return ListView.builder(
              itemBuilder: (ctx, pos) {
                return getListElement(listDevs[pos], context: ctx);
              },
              itemCount: listDevs.length,
            );
          } else {
            if (response.hasError) {
              print('ResponseError: ${response.error}');
              return ListView(
                children: getListWidgets(),
              );
            } else {
              return WaitingBar();
            }
          }
        },
        initialData: CircularProgressIndicator(),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
