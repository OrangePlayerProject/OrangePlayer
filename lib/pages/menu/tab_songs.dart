import 'package:flutter/material.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/net/beans/Song.dart';
import 'package:orange_player/pages/components/waiting_bar.dart';
import 'package:orange_player/pages/player/track_cover.dart';
import 'package:orange_player/sys/GlobalObject.dart';

class TabSongs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TabSongsState();
  }
}

// -------------- STATE -----------------

class _TabSongsState extends State<TabSongs>
    with AutomaticKeepAliveClientMixin<TabSongs> {
  MusicConsumer consumer;
  Future<List<Song>> listSongs;

  _TabSongsState() {
    consumer = MusicConsumer();
    listSongs = consumer.getFullSongs();
    GlobalObject.setGlobalObjectsAsync(consumer);
  }

  Widget getListElement(Song song, {BuildContext context}) {
    String title = song.title;
    String artist = song.artist;
    if (artist == null) artist = "Artista Desconocido";

    final int titleLimit = 30;

    /*if (song.hasCover()) {
      final directory = Directory.systemTemp.createTemp();

      directory.whenComplete(() {
        directory.then((dir) {
          const MethodChannel('plugins.flutter.io/path_provider')
              .setMockMethodCallHandler((MethodCall methodCall) async {
            if (methodCall.method == 'getApplicationDocumentsDirectory') {
              return dir.path;
            }
            return null;
          });

          File file = File('${dir.path}/$title');
          file.createSync();
          print('File: ${file.path}');
          print('FileExists: ${file.existsSync()}');
          file.writeAsBytesSync(song.coverData, flush: true);
        });
      });
    }*/

    //print("PixelRatio (Pixeles fisicos por cada pixel logico): ${MediaQuery.of(context).devicePixelRatio}");
    int index = song.trackIndex;
    return Card(
      borderOnForeground: true,
      elevation: 3,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 5, top: 5),
      child: InkWell(
        onTap: () {
          consumer.play([index.toString()]);
          GlobalObject.setGlobalObjectsAsync(consumer);
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10, right: 20),
              width: 40,
              height: 40,
              child: TrackCover(
                  imgSource: song.hasCover() ? song.coverData : "cd.png"),
            ),
            Column(
              children: <Widget>[
                Text(
                  title.length > titleLimit
                      ? title.substring(0, titleLimit - 3) + "..."
                      : title,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                ),
                Text(
                  artist.length > titleLimit
                      ? artist.substring(0, titleLimit)
                      : artist,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                )
              ],
            ),
            /*MaterialButton(
              child: Icon(Icons.menu),
              onPressed: () {
                print("xd");
              },
            )*/
          ],
        ),
      ),
    );
  }

  List<Widget> getListWidgets({List<Song> listSongs, BuildContext context}) {
    if (listSongs == null)
      return List();
    else {
      return listSongs.map((song) {
        return getListElement(song, context: context);
      }).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Align(
      alignment: Alignment.center,
      child: FutureBuilder(
        future: listSongs,
        builder: (context, response) {
          ConnectionState state = response.connectionState;
          if (state == ConnectionState.done && response.hasData) {
            List<Song> listSongs = response.data;
            return ListView.builder(
              itemBuilder: (ctx, pos) {
                return getListElement(listSongs[pos], context: ctx);
              },
              itemCount: listSongs.length,
            );
          } else {
            if (response.hasError) {
              print('ResponseError: ${response.error}');
              return ListView(
                children: getListWidgets(),
              );
            } else {
              return WaitingBar();
            }
          }
        },
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
