import 'package:flutter/material.dart';
import 'package:orange_player/sys/SysInfo.dart';

class WaitingBar extends StatelessWidget {
  const WaitingBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      backgroundColor: SysInfo.PRIMARY_COLOR,
      strokeWidth: 5,
    );
  }
}
