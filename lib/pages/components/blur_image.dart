import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:ui';

import 'Blurred.dart';

class BlurImage extends StatelessWidget {
  final dynamic imgSrc;
  final double width;
  final double height;

  BlurImage({this.imgSrc, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    print("isString: ${imgSrc.runtimeType == String}");
    ImageProvider img = imgSrc.runtimeType == String
        ? AssetImage(imgSrc.toString())
        : MemoryImage(imgSrc as Uint8List);

    return Blurred(
      sigmaX: 8,
      sigmaY: 8,
      widget: Image(
        image: img,
        fit: BoxFit.fill,
      ),
    );
  }
}
