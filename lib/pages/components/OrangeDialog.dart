import 'package:flutter/material.dart';
import 'package:orange_player/sys/SysInfo.dart';

class OrangeDialog extends StatelessWidget {
  final String title;
  final String content;

  const OrangeDialog({Key key, this.title, this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final elementsColor = SysInfo.PRIMARY_COLOR;
    final backgroundColor = Colors.white;

    return AlertDialog(
      title: Text(
        title,
        textAlign: TextAlign.center,
      ),
      content: Text(
        content,
        textAlign: TextAlign.center,
      ),
      actions: <Widget>[
        FlatButton(
          color: elementsColor,
          textColor: backgroundColor,
          padding: EdgeInsets.only(
            left: 40,
            right: 40,
          ),
          child: Text(
            "OK",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 14),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
      backgroundColor: backgroundColor,
      titleTextStyle: TextStyle(
        color: elementsColor,
        fontSize: 24,
        fontWeight: FontWeight.w700,
      ),
      contentTextStyle: TextStyle(
        color: elementsColor,
        fontSize: 18,
        fontWeight: FontWeight.w500,
      ),
      elevation: 20,
      titlePadding: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 15,
        top: 5,
      ),
      contentPadding: EdgeInsets.only(
        left: 10,
        right: 10,
      ),
    );
  }
}
