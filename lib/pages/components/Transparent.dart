import 'package:flutter/material.dart';

class Transparent extends StatelessWidget {
  final double opacity;
  final Widget child;

  const Transparent({this.opacity, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white.withOpacity(opacity),
      child: child,
    );
  }
}
