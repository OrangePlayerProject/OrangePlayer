import 'package:flutter/material.dart';

import 'blur_image.dart';

class Backgrounded extends StatelessWidget {
  final Positioned child;
  final String imgSrc;
  final bool blurred;
  final double width;
  final double height;
  final double top;
  final double left;

  const Backgrounded(
      {Key key,
      this.child,
      this.imgSrc,
      this.blurred,
      this.width,
      this.height,
      this.top,
      this.left})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (width == null || height == null || top == null || left == null) {
      return Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            child: blurred
                ? BlurImage(imgSrc: imgSrc)
                : Image(
                    image: AssetImage(imgSrc),
                    fit: BoxFit.fill,
                  ),
          ),
          child
        ],
      );
    } else {
      return Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: width,
            height: height,
            top: top,
            left: left,
            child: blurred
                ? BlurImage(imgSrc: imgSrc)
                : Image(
                    image: AssetImage(imgSrc),
                    fit: BoxFit.fill,
                  ),
          ),
          child
        ],
      );
    }
  }
}
