import 'package:flutter/material.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/net/SystemConsumer.dart';
import 'package:orange_player/sys/SysInfo.dart';
import 'package:orange_player/pages/menu/menu_drawer.dart';
import 'package:orange_player/pages/menu/tab_drives.dart';
import 'package:orange_player/pages/menu/tab_folders.dart';
import 'package:orange_player/pages/menu/tab_songs.dart';

class MenuPage extends StatefulWidget {
  MusicConsumer _consumer;

  MenuPage() {
    _consumer = MusicConsumer();
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MenuPageState();
  }
}

class _MenuPageState extends State<MenuPage> {
  @override
  void initState() {
    super.initState();
    //DisplayUtil.hideStatusBar();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        primary: true,
        appBar: _getAppBar(),
        drawer: MenuDrawer(),
        body: TabBarView(
          children: <Widget>[
            TabSongs(),
            TabFolders(),
            TabDrives(),
          ],
        ),
      ),
    );
  }

  AppBar _getAppBar() {
    return AppBar(
      title: const Text(SysInfo.APP_NAME),
      elevation: 20,
      actions: <Widget>[
        IconButton(
          icon: Image.asset(
            "assets/img/orange.png",
            height: 30,
            width: 30,
          ),
          onPressed: () {
            /*Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => PlayerWidget(
                                current: ,
                              ),
                        ),
                      );*/
            Navigator.of(context).pushNamed("/player");
          },
        ),
        IconButton(
          icon: Icon(
            Icons.power_settings_new,
            size: 32,
          ),
          onPressed: () {
            SystemConsumer cons = SystemConsumer();
            cons.shutdownSystem();
            Navigator.popUntil(
                context, ModalRoute.withName(Navigator.defaultRouteName));
          },
        ),
        IconButton(
          icon: Icon(
            Icons.exit_to_app,
            size: 32,
          ),
          onPressed: () {
            Navigator.popUntil(
                context, ModalRoute.withName(Navigator.defaultRouteName));
          },
        ),
      ],
      bottom: TabBar(
        isScrollable: false,
        tabs: <Widget>[
          Tab(
            text: "PLAYLIST",
          ),
          Tab(
            text: "CARPETAS",
          ),
          Tab(
            text: "DISPOSITIVOS",
          ),
        ],
      ),
    );
  }
}
