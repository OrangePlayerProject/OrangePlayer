import 'dart:core';

import 'package:flutter/material.dart';
import 'package:orange_player/net/NetworkConsumer.dart';
import 'package:orange_player/sys/DisplayUtil.dart';
import 'package:orange_player/sys/SysInfo.dart';

class ConnectPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ConnectPageState();
  }
}

class _ConnectPageState extends State<ConnectPage> {
  NetworkConsumer _consumer;
  String _cboVal;
  String _passwVal;

  Future<List<DropdownMenuItem>> getNetworkItems() async {
    List<String> listNets =
        (await _consumer.getNetworks()).map((net) => net.name).toList();
    print("ListNets: ${listNets}");

    return listNets
        .map((net) => DropdownMenuItem<String>(value: net, child: Text(net)))
        .toList();
  }

  @override
  void initState() {
    super.initState();
    _consumer = NetworkConsumer();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    double contHeight = DisplayUtil.getHeight(context, perc: 35);
    double contWidth = DisplayUtil.getWidth(context, perc: 85);

    return Scaffold(
      appBar: AppBar(
        title: Text("Conectar a"),
      ),
      body: Container(
        color: Colors.black87,
        child: Align(
          alignment: Alignment.center,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Container(
                color: Colors.white,
                width: contWidth,
                height: contHeight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      child: FutureBuilder(
                        future: getNetworkItems(),
                        builder: (ctx, response) {
                          try {
                            List<DropdownMenuItem> items = response.requireData;
                            return DropdownButton(
                              hint: Text("Red Wifi: "),
                              isExpanded: true,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              items: items,
                              value: _cboVal,
                              onChanged: (value) {
                                setState(() {
                                  _cboVal = value;
                                });
                              },
                            );
                          } catch (e) {
                            return DropdownButton(
                              hint: Text("Red Wifi: "),
                              isExpanded: true,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              items: List(),
                              onChanged: (value) {},
                            );
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(hintText: "Password: "),
                        onChanged: (value) {
                          _passwVal = value;
                        },
                      ),
                    ),
                    SizedBox(
                      child: RaisedButton(
                          child: Text(
                            "Conectar",
                            style: TextStyle(fontSize: 18),
                          ),
                          color: SysInfo.PRIMARY_COLOR,
                          textColor: Color(0xffffffff),
                          padding: EdgeInsets.only(
                              left: 40, right: 40, top: 10, bottom: 10),
                          onPressed: () async {
                            if (_cboVal != null && _passwVal != null) {
                              print("Waiting for connection response...");
                              bool connected = await _consumer.connectWifi(
                                  _cboVal, _passwVal);
                              print("Connected: $connected");
                            }
                          }),
                    )
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
