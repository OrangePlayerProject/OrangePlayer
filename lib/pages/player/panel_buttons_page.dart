import 'package:flutter/material.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/pages/player_page.dart';
import 'package:orange_player/sys/DisplayUtil.dart';
import 'package:orange_player/pages/components/Transparent.dart';
import 'package:orange_player/sys/GlobalObject.dart';

class PanelButtonsWidget extends StatefulWidget {
  PlayerPageState _PlayerPageState;
  MusicConsumer _consumer;

  PanelButtonsWidget(PlayerPageState state) {
    _PlayerPageState = state;
    _consumer = state.consumer;
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PanelButtonsWidgetState(_PlayerPageState, _consumer);
  }
}

class PanelButtonsWidgetState extends State<PanelButtonsWidget> {
  PlayerPageState _PlayerPageState;
  MusicConsumer _consumer;
  bool _isPaused;
  Image _playIcon;

  GlobalKey _btnPlayKey;

  PanelButtonsWidgetState(
      PlayerPageState PlayerPageState, MusicConsumer consumer) {
    _PlayerPageState = PlayerPageState;
    _consumer = consumer;
    _isPaused = false;
    _playIcon = Image.asset("assets/img/pause.png");
  }

  void initState() {
    super.initState();
    _consumer = MusicConsumer();
    _btnPlayKey = GlobalKey();
  }

  void _configPlayIcon(bool paused) {
    _playIcon = paused
        ? Image.asset("assets/img/play.png")
        : Image.asset("assets/img/pause.png");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: DisplayUtil.getHeight(context, perc: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Transparent(
            opacity: 0.4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  icon: Image.asset("assets/img/menu.png"),
                  onPressed: () {
                    setState(() {
                      var nav = Navigator.of(context);
                      nav.pop();
                    });
                  },
                ),
                IconButton(
                    icon: Image.asset("assets/img/seekp.png"),
                    onPressed: () {
                      _consumer.prev();
                      GlobalObject.setGlobalObjectsDefault();
                      _PlayerPageState.loadSong();
                    }),
                FutureBuilder(
                  future: _consumer.isPlayerPaused(),
                  builder: (ctx, snapshot) {
                    bool isDone =
                        snapshot.connectionState == ConnectionState.done &&
                            snapshot.hasData;
                    if (isDone) {
                      bool isPaused = snapshot.data;
                      _configPlayIcon(isPaused);
                    }
                    return IconButton(
                      key: _btnPlayKey,
                      icon: _playIcon,
                      onPressed: () async {
                        bool paused = await _consumer.isPlayerPaused();
                        if (paused) {
                          _consumer.resume();
                          _configPlayIcon(false);
                        } else {
                          _consumer.pause();
                          _configPlayIcon(true);
                        }
                        setState(() {});
                      },
                    );
                  },
                ),
                IconButton(
                    icon: Image.asset("assets/img/seekn.png"),
                    onPressed: () {
                      _consumer.next();
                      GlobalObject.setGlobalObjectsDefault();
                      _PlayerPageState.loadSong();
                    }),
                IconButton(
                  icon: Image.asset("assets/img/shuffle.png"),
                  onPressed: () {},
                ),
              ],
            )),
      ),
    );
  }
}
