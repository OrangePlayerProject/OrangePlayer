import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:orange_player/sys/SysInfo.dart';

class TrackCover extends StatelessWidget {
  final imgSource;
  final double width;
  final double height;

  TrackCover({this.imgSource, this.width = 300, this.height = 300});

  ImageProvider _getImageFromSource() {
    if (imgSource == null)
      return MemoryImage(Uint8List(0));
    else if (imgSource.runtimeType.toString().contains("String"))
      return AssetImage("assets/img/${imgSource.toString()}");
    else
      return MemoryImage(
        imgSource,
      );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(),
          image: DecorationImage(
            image: _getImageFromSource(),
            fit: BoxFit.fill,
            alignment: Alignment(0, 0),
          ),
        ));
  }
}
