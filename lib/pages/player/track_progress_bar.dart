import 'package:flutter/material.dart';
import 'package:fluttery_seekbar/fluttery_seekbar.dart';
import 'package:orange_player/pages/player_page.dart';

class TrackProgressBar extends StatefulWidget {
  PlayerPageState _pageState;

  TrackProgressBar(PlayerPageState pageState) {
    this._pageState = pageState;
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TrackProgressBarState(_pageState);
  }
}

class _TrackProgressBarState extends State<TrackProgressBar> {
  double _progress;
  PlayerPageState _pageState;

  _TrackProgressBarState(PlayerPageState pageState) {
    this._pageState = pageState;
    _progress = 0;
  }

  @override
  void initState() {
    super.initState();
    _pageState.consumer.getGain().then((gain) {
      setState(() {
        _progress = gain / 10;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return RadialSeekBar(
      trackColor: Colors.black,
      trackWidth: 4.0,
      progressColor: Colors.red,
      progressWidth: 10.0,
      progress: _progress,
      thumb: CircleThumb(
        color: Colors.red,
        diameter: 20.0,
      ),
      thumbPercent: _progress,
      onDragUpdate: (double percent) {
        setState(() {
          _progress = percent;
          _pageState.consumer.setGain(_progress * 100);
        });
      },
    );
  }
}
