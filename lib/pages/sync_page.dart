import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:orange_player/net/ConnectionManager.dart';
import 'package:orange_player/net/MusicConsumer.dart';
import 'package:orange_player/sys/DisplayUtil.dart';
import 'package:orange_player/sys/SysInfo.dart';
import 'package:orange_player/pages/components/OrangeDialog.dart';

class SyncPage extends StatefulWidget {
  @override
  _SyncPageState createState() => _SyncPageState();
}

class _SyncPageState extends State<SyncPage> {
  String _host;

  _SyncPageState() {
    _host = "";
    DisplayUtil.enterFullscreen();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      primary: true,
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: SysInfo.PRIMARY_COLOR,
        child: Wrap(
          alignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Container(
              height: DisplayUtil.getHeight(context, perc: 5, withBar: false),
            ),
            Image.asset(
              "assets/img/orange2.png",
              width: 512,
              height: 350,
            ),
            Container(
              width: DisplayUtil.getWidth(context, perc: 67),
              height: 40,
              color: Colors.white,
              child: TextField(
                controller: TextEditingController(
                  text: _host,
                ),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "IP OrangeBox",
                ),
                style: TextStyle(
                  color: SysInfo.PRIMARY_COLOR,
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
                keyboardType: TextInputType.phone,
                onChanged: (s) => _txtHostOnChanged(s),
                onSubmitted: (s) async => _txtHostOnSubmitted(context, s),
              ),
            ),
            /*Container(
              width: DisplayUtil.getWidth(context, perc: 70),
              height: 65,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Padding(
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 10,
                    right: 10,
                  ),
                  child: MaterialButton(
                    onPressed: () async {
                      String ip = _host.isEmpty ? SysInfo.SERVER_HOST : _host;
                      if (await ConnectionManager.isConnected(ip)) {
                        SysInfo.SERVER_HOST = ip;
                        MusicConsumer consumer = MusicConsumer();
                        consumer.startPlayer();
                        Navigator.pushNamed(context, "/menu");
                      } else {
                        var dialog = OrangeDialog(
                          title: "Error",
                          content: "No es posible conectar a ${ip}",
                        );
                        showDialog(
                          context: context,
                          builder: (BuildContext ctx) => dialog,
                        );
                      }
                    },
                    color: Colors.white,
                    child: Text(
                      "Conectar",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 30,
                        color: SysInfo.PRIMARY_COLOR,
                      ),
                    ),
                  ),
                ),
              ),
            )*/
          ],
        ),
      ),
    );
  }

  Future _txtHostOnSubmitted(BuildContext context, String s) async {
    _host = s;
    String ip = _host.isEmpty ? SysInfo.SERVER_HOST : _host;
    if (await ConnectionManager.isConnected(ip)) {
      SysInfo.SERVER_HOST = ip;
      MusicConsumer consumer = MusicConsumer();
      consumer.startPlayer();
      Navigator.pushNamed(context, "/menu");
    } else {
      var dialog = OrangeDialog(
        title: "Error",
        content: "No es posible conectar a ${ip}",
      );
      showDialog(
        context: context,
        builder: (BuildContext ctx) => dialog,
      );
    }
  }

  void _txtHostOnChanged(String s) {
    _host = s;
  }
}
