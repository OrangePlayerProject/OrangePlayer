import 'package:flutter/material.dart';
import 'package:orange_player/pages/connect_page.dart';
import 'package:orange_player/pages/menu_page.dart';
import 'package:orange_player/pages/player_page.dart';
import 'package:orange_player/pages/sync_page.dart';
import 'package:orange_player/sys/SysInfo.dart';

class OrangePlayer extends StatelessWidget {
  const OrangePlayer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: SysInfo.APP_NAME,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: SysInfo.PRIMARY_COLOR,
        accentColor: SysInfo.PRIMARY_COLOR,
        backgroundColor: Colors.white,
      ),
      routes: {
        '/': (context) => SyncPage(),
        '/connect': (context) => ConnectPage(),
        '/menu': (context) => MenuPage(),
        '/player': (context) => PlayerPage(),
      },
    );
  }
}
